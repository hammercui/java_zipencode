import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import java_zipencode.StartDecodeListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;



public class FileFrame extends JFrame {
	 private JTextField labFilePath;//文件路径
	 private JButton btnOpenFile; //打开文件按钮
	 private JButton btnStartEncode;//开始加密
	 private JButton btnStartDecode;//开始解密
	 private JTextArea labConsole;//输出
	 
	 public FileFrame()
	 {
		 super();
		 this.setSize(800, 700);
		 
		 this.getContentPane().setLayout(null);//设置布局控制器
		 
		 this.add(this.getFilePathText(),null);//
		 this.add(this.getBtnOpenFile(),null); //
		 this.add(this.getBtnStartEncode(),null);
		 this.add(this.getBtnStartDecode(),null);
		 this.add(this.getFileConsole(),null);//
		 //添加加密事件监听
		 StartEncodeListener listener = new StartEncodeListener();
		 listener.init(labFilePath,labConsole);
		 btnStartEncode.addActionListener(listener);//添加监听器类，其主要的响应都由监听器类的方法实现
		 //添加解密事件监听
		 StartDecodeListener delistener = new StartDecodeListener();
		 delistener.init(labFilePath, labConsole);
		 btnStartDecode.addActionListener(delistener);
	 }
	 
	 /**
	  * 获得文件路径文本
	  * @return
	  */
	 private JTextField getFilePathText()
	 {
		if (labFilePath==null) {
			labFilePath =  new JTextField();
			labFilePath.setBounds(10,20,250,20);
		} 
		 return labFilePath;
	 }
	 
	 private  JButton getBtnOpenFile()
	 {
		 btnOpenFile = new JButton();
		 btnOpenFile.setBounds(10,60,100,27);
		 btnOpenFile.setText("打开文件");
		 btnOpenFile.setToolTipText("OK");
		 btnOpenFile.addActionListener(new ClickButton());//添加监听器类，其主要的响应都由监听器类的方法实现
		 return btnOpenFile;
	 }
	 
	 private JButton getBtnStartEncode()
	 {
		 btnStartEncode = new JButton();
		 btnStartEncode.setBounds(130,60,100,27);
		 btnStartEncode.setText("ok");
		 btnStartEncode.setToolTipText("OK");
		 return btnStartEncode;
	 }
	 private JButton getBtnStartDecode()
	 {
		 btnStartDecode = new JButton();
		 btnStartDecode.setBounds(250,60,100,27);
		 btnStartDecode.setText("bk");
		 btnStartDecode.setToolTipText("bK");
		 return btnStartDecode;
	 }
	 /**
	     * 监听器类实现ActionListener接口，主要实现actionPerformed方法
	     * @author HZ20232
	     *
	     */
	    private class ClickButton implements ActionListener{
	        public void actionPerformed(ActionEvent e){
	        	//弹出文件夹选择框
	        	JFileChooser jfc=new JFileChooser();  
	            jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES );  
	            jfc.showDialog(new JLabel(), "选择文件路径");  
	            File file=jfc.getSelectedFile();  
	            if(file.isDirectory()){  
	                //文件夹名字jfc.getSelectedFile().getName()
	                String pathString = file.getAbsolutePath();
		            System.out.println("文件夹路径："+pathString); 
		            labFilePath.setText(pathString);
	            }else if(file.isFile()){  
	                System.out.println("打开的是文件，请打开文件夹");  
	            }  
	        }
	    }
	    /**
	     * 开始加密的响应
	     * @author hammerCui
	     * @time 2015年12月23日
	     * @descrpition:
	     */
	   
	    
	 private JTextArea getFileConsole()
	 {
		 if (labConsole==null) {
			 labConsole =  new JTextArea();
			 labConsole.setBounds(10,140,600,500);
			 labConsole.setLineWrap(true);//激活自动换行功能 
			} 
		 return labConsole;
	 }
}
