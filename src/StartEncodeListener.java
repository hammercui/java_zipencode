import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import java_zipencode.ZipCipherUtil;

import javax.swing.JTextArea;
import javax.swing.JTextField;

public class StartEncodeListener implements ActionListener{
	
	private JTextField labFilePath;
	private JTextArea labConsole;
	
	public void init(JTextField  textfile,JTextArea area) {
		labFilePath = textfile;
		labConsole = area;
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		//遍历文件并加密
		File root = new File(labFilePath.getText());
		showAllFiles(root);
	}
	

	//查找所有文件
	private void showAllFiles(File dir)
	{
		File[] fs = dir.listFiles();
		for(int i=0; i<fs.length; i++)
		  {
			if (fs[i].isDirectory()) {//是文件夹
				try{
	    		     showAllFiles(fs[i]);
	    		    }catch(Exception e)
	    		    	{System.out.println("e:"+e.toString());}
			}
			else{//是文件
				String filenameString = fs[i].getAbsolutePath();
				int qianzhuiCount = filenameString.lastIndexOf(".");
				String qianzhui = filenameString.substring(0,qianzhuiCount);
	    		String houzhui  = filenameString.substring(qianzhuiCount + 1, filenameString.length());
				if (houzhui.equals("jpg")
							||houzhui.equals("JPG")
							||houzhui.equals("png")
							||houzhui.equals("PNG")
							||houzhui.equals("gif")){
						encodeFile(qianzhui,houzhui);
						//System.out.println("文件："+filenameString);
			    		labConsole.setCaretPosition(labConsole.getText().length());  
			    		labConsole.append(filenameString+"\n");
				}
				
			}
		 }
	}
	
	//加密某个问价
	private void encodeFile(String qianzhui,String houzhui)
	{
		try {
			ZipCipherUtil.getIns().encryptZip(qianzhui+"."+houzhui, qianzhui+".temp", "hammercui");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
